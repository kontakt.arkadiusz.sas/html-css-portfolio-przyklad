// 1. Dodanie obsługi klikania w przycisk ✅
//    (odbiór eventu kliknięcia)
const clickMeButton = document.querySelector("#counter-button");
// button.addEventListener(jaki event?, co się ma stać?)
clickMeButton.addEventListener("click", () => {
    
    // 2. Pobranie wartości z licznika ✅
    const counterDivContent = document.querySelector("#counter").innerHTML;
    const newValue = Number(counterDivContent) + 1;

    // 3. Zmodyfikowanie wartości w liczniku (+1) ✅
    document.querySelector("#counter").innerHTML = newValue;
});

// JSON
const client = {
    name: "Adam",
    address: "Długa 1",
    orderId: 1017
}

fetch('https://api.open-meteo.com/v1/forecast?latitude=52.52&longitude=13.41&current_weather=true&hourly=temperature_2m,relativehumidity_2m,windspeed_10m')
  .then((response) => response.json())
  .then((data) => console.log(data));